use std::fmt::Write;

use rust_nea::{format_response, Request};

fn main() -> std::io::Result<()> {
    nea::run_request_handler(handler)
}

async fn handler(
    _bucket_index: nea::index::BucketIndex,
    tcp_stream: nea::net::TcpStream,
) -> std::io::Result<()> {
    let mut buf = [0; 1024];

    // leaving 8 zero bytes for future RocStr optimization
    let n = tcp_stream.read(&mut buf).await.unwrap();
    let string = std::str::from_utf8(&buf[..n]).unwrap();

    let request = Request::parse(string).unwrap();
    let response = respond(&request);

    let _ = tcp_stream.write(&response).await.unwrap();

    Ok(())
}

fn respond(request: &Request) -> Vec<u8> {
    let path_string = request
        .body
        .lines()
        .filter_map(|line| line.split_once(", "))
        .map(|(x, y)| {
            (
                x.trim().parse::<u32>().unwrap(),
                y.trim().parse::<u32>().unwrap(),
            )
        })
        .fold(String::from("M 0 0 L"), |mut acc, (x, y)| {
            write!(acc, "{} {} ", x, y).expect("failed to write to string");
            acc
        });

    let response = format!(
        r#"<svg width="100" height="100" xmlns="http://www.w3.org/2000/svg">
    <path d="{path_string}" stroke="black" fill="transparent"/>
</svg>
"#
    );
    format_response(&response)
}
