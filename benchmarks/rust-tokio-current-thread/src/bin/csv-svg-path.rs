use std::fmt::Write;

use expry::memorypool::*;

use rust_tokio::{
    request::RequestBuf,
    response::{ContentType, Response, StatusCode},
    server,
};

#[tokio::main]
async fn main() {
    server::serve(average).await.unwrap();
}

pub fn average(request: RequestBuf, pool: &mut MemoryScope<'_>) -> Response {
    let request = request.as_request().unwrap();

    let path = request
        .body
        .lines()
        .filter_map(|line| line.split_once(", "))
        .map(|(x, y)| {
            (
                x.trim().parse::<u32>().unwrap(),
                y.trim().parse::<u32>().unwrap(),
            )
        })
        .fold(String::from("M 0 0 L"), |mut acc, (x, y)| {
            write!(acc, "{} {} ", x, y).expect("failed to write to string");
            acc
        });

    let response = format!(
        r#"<svg width="100" height="100" xmlns="http://www.w3.org/2000/svg">
    <path d="{path}" stroke="black" fill="transparent"/>
</svg>"#
    );
    Response {
        content_type: ContentType::IMAGE_SVG,
        body: response,
        status: StatusCode::OK,
    }
}
