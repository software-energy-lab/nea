use rust_tokio::{request::RequestBuf, response::Response, server};

use expry::memorypool::*;

#[tokio::main]
async fn main() {
    server::serve(unfavorable).await.unwrap();
}

pub fn unfavorable(request: RequestBuf, pool: &mut MemoryScope<'_>) -> Response {
    let _request = request.as_request().unwrap();
    let response = include_str!("../../../../Cargo.toml");
    response.into()
}
