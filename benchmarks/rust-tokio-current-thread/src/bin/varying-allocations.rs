use rust_tokio::{request::RequestBuf, response::Response, server};

use expry::memorypool::*;

#[tokio::main(flavor = "current_thread")]
async fn main() {
    server::serve(favorable).await.unwrap();
}

pub fn favorable(request: RequestBuf, pool: &mut MemoryScope<'_>) -> Response {
    let request = request.as_request().unwrap();

    let n = request.path[1..].parse::<usize>().unwrap();
    let capacity = n * 1024;

    let v : &mut [std::mem::MaybeUninit<u8>] = pool.alloc(capacity);
    v.fill(std::mem::MaybeUninit::new(0xAAu8));
    let v : &[u8] = unsafe { std::mem::transmute(v) };
    let response = format!("{}", v.len());
    response.into()
}
