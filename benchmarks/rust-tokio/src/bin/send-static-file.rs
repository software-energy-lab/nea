use rust_tokio::{request::RequestBuf, response::Response, server};

#[tokio::main]
async fn main() {
    server::serve(unfavorable).await.unwrap();
}

pub async fn unfavorable(request: RequestBuf) -> Response {
    let _request = request.as_request().unwrap();
    let response = include_str!("../../../../Cargo.toml");
    response.into()
}
