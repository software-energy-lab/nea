use rust_tokio::{request::RequestBuf, response::Response, server};

#[tokio::main]
async fn main() {
    server::serve(favorable).await.unwrap();
}

pub async fn favorable(request: RequestBuf) -> Response {
    let request = request.as_request().unwrap();

    let n = request.path[1..].parse::<usize>().unwrap();
    let capacity = n * 1024;

    let v = vec![0xAAu8; capacity];
    let response = format!("{}", v.len());
    response.into()
}
