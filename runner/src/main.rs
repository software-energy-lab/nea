mod probes;
mod usertime;

use probes::NeaProbes;

use std::process::{Child, Command};
use std::thread::sleep;
use std::time::Duration;

use energy_bench::EnergyBenchBuilder;

const PATHS: &[(&str, &str)] = &[
    ("go", ""),
    ("roc-nea", ""),
    ("rust-nea", "/target/release"),
    ("rust-tokio", "/target/release"),
    ("rust-tokio-current-thread", "/target/release"),
];

const EXAMPLES: &[&str] = &[
    "csv-svg-path",
    "send-static-file",
    "varying-allocations"
];

fn main() {
    let mut builder = EnergyBenchBuilder::new("nea")
        .with_number_of_measurements(100)
        .with_idle_duration(300);
    builder.probes(|| Box::new(NeaProbes::get_available()));

    let mut bench = builder.build();

    for (dir, subdir) in PATHS {
        for example in EXAMPLES {
            let mut server = spawn_server(dir, subdir, example);
            // Wait for server to be initialized
            sleep(Duration::from_secs(3));

            bench.benchmark(
                &format!("{}:{}", dir, example),
                &|| init_siege(example),
                &|mut siege| siege.output()
            );

            server.kill().unwrap();
        }
    }
}

fn spawn_server(dir: &str, subdir: &str, example: &str) -> Child {
    Command::new(&format!("benchmarks/{}{}/{}", dir, subdir, example))
        .env("RUST_LOG", "error")
        .spawn()
        .unwrap()
}

fn init_siege(example: &str) -> Command {
    let f = format!("benchmarks/{}.txt", example);
    let mut siege = Command::new("siege");
    siege.args(&["-i", "-j", "-c", "4", "-r", "5000", "-f", &f]);
    siege
}
