use energy_bench::Probes;
use rapl_energy::*;

use crate::usertime::UserTime;

pub struct NeaProbes {
    probes: Vec<Box<dyn Energy>>,
}

impl NeaProbes {
    pub fn get_available() -> Self {
        let v = vec![
            ina(),
            Msr::now(),
            Rapl::now(),
            Some(UserTime::now())
        ];
        let probes = v.into_iter().filter_map(|x| x).collect();
        Self { probes }
    }
}

impl Probes for NeaProbes {
    fn elapsed(&self) -> ProbeEnergy {
        self.probes
            .iter()
            .rev()
            .map(|probe| probe.elapsed())
            .flatten()
            .collect()
    }

    fn reset(&mut self) {
        self.probes
            .iter_mut()
            .for_each(|probe| probe.reset());
    }
}

fn ina() -> Option<Box<dyn Energy>> {
    let path = std::env::var("ENERGY_STATS").ok()?;
    let header = "X-Electricity-Consumed-Total".to_string();
    Http::now(path, header)
}
