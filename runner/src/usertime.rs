use cpu_time::ProcessTime;
use indexmap::indexmap;
use rapl_energy::Energy;

pub struct UserTime {
    now: ProcessTime,
}

impl UserTime {
    pub fn now() -> Box<dyn Energy> {
        Box::new(Self { now: ProcessTime::now() })
    }
}

impl Energy for UserTime {
    fn elapsed(&self) -> rapl_energy::ProbeEnergy {
        let sec = self.now.elapsed().as_secs_f32();
        indexmap!{
            "usertime".to_string() => sec,
        }
    }

    fn reset(&mut self) {
        self.now = ProcessTime::now();
    }
}
